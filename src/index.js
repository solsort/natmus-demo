import React from "react";
import ReactDOM from "react-dom";
import axios from "axios";

import { withStyles } from "@material-ui/styles";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import Container from "@material-ui/core/Container";
import Drawer from "@material-ui/core/Drawer";
import FormGroup from "@material-ui/core/FormGroup";
import FormLabel from "@material-ui/core/FormLabel";
import RadioGroup from "@material-ui/core/RadioGroup";
import Radio from "@material-ui/core/Radio";
import Switch from "@material-ui/core/Switch";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";

import categories from "./results.json";
import _ from "lodash";

for (let key in categories) {
  categories[key] = categories[key].split(" ");
}
console.log(categories);

const styles = {};

class NatmusDemo extends React.Component {
  constructor() {
    super();
    this.state = {
      tab: 2
    }
  }
  render() {
    const { state, props } = this;
    const { classes } = props;
    const value = state.tab;
    const imgs = Object.values(categories)[value];
    return (
      <div>
        <AppBar position="static">
          <Tabs
            value={value}
            onChange={(o, tab) => this.setState({tab})}
            aria-label="simple tabs example"
          >
            {Object.keys(categories).map(name => <Tab label={name} key={name} />) }
          </Tabs>
        </AppBar>
        <div style={{textAlign: "justify"}}>
        {_.shuffle(imgs).slice(0,120).map(s => {
          let [collection, id] = s.split('-');
          return <span><img style={{padding:10}} src={`https://devsamlinger.natmus.dk/${collection}/asset/${id}.jpg`} height="200" /> {" "}</span>

        })}
        </div>
      </div>
    );
  }
}

const StyledNatmusDemo = withStyles(styles)(NatmusDemo);

document.addEventListener("DOMContentLoaded", async () => {
  let elem = document.getElementById("recommend-admin");
  if (!elem) {
    elem = document.createElement("div");
    elem.id = "recommend-admin";
    document.body.appendChild(elem);
  }
  ReactDOM.render(<StyledNatmusDemo />, elem);
});
